// SHORTHAND OUTPUT
const l = (arg) => console.log(arg);
// EOF SHORTHAND OUTPUT

let employees = [
    {
        "name": "Thonie Fernandez",
        "department": "Instructor",
        "yearEmployed": "2020",
        "ratings": 5.0
    },
    {
        "name": "Charles Quimpo",
        "department": "Instructor",
        "yearEmployed": "2010",
        "ratings": 5.0
    },
    {
        "name": "Sam",
        "department": "Intructress",
        "yearEmployed": "2013",
        "ratings": 4.0
    },
    {
        "name": "Reggie",
        "department": "HR",
        "yearEmployed": "2019",
        "ratings": 4.0
    }
];

let user = [
    {
    "name": "Mon",
    "favouriteNumber": 7,
    "isProgrammer": true,
    "hobbies": [
        "Weightlifting",
        "Reading",
        "Listening to music"
    ],
    "friends": [
        {
            "name": "Roy",
            "isProgrammer": true
        },
        {
            "name": "Marco",
            "isProgrammer": false
        },
        {
            "name": "Alphonse",
            "isProgrammer": true
        }
    ]


}

];

l(employees);
l(user);


let application = `{
    "name": "javascript server",
    "version": "1.0",
    "description": "server side application done using javascript and node js",
    "main": "index.js",
    "scripts": {
        "start": "nodes index.js"
    },
    "keywords": [
        "server",
        "node",
        "backend"
    ],
    "author": "RRC",
    "license": "ORMRRC-2022"
}`;

l(user[0])
l(JSON.parse(application).description)
l(typeof application)